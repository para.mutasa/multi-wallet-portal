<?php
   session_start();
   include_once('../utils/MultiCashUtility.php');
    if(isset($_POST['login']))
    {
        if(!empty($_POST['vEmail']) && !empty($_POST['vPassword']))
        {
            $email 		 = trim($_POST['vEmail']);
            $password 	 = trim($_POST['vPassword']);

            if(!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
                $errorMsg = 'reCAPTHCA verification failed, please try again.';
            }else {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($response);

                if($response->success) {
                    $tbl = 'administrators';
                    $sql = "SELECT * FROM $tbl  WHERE vEmail = '".$email."'";
                    $db_login = $db_conn->MySQLSelect($sql);
                    if(count($db_login) > 0){
                        $hash = $db_login[0]['vPassword'];
                        $checkValid = $generalobj->check_password($password, $hash);
                        if($checkValid == 0){
                            $errorMsg = "Invalid Password";
                        }
                        if($db_login[0]['eStatus'] != 'Active'){
                            $errorMsg = "User Not Active";
                        } else {
                            $_SESSION['sess_iAdminId'] = $db_login[0]['iAdminId'];
                            $_SESSION["sess_vFirstName"] = $db_login[0]['vFirstName'];
                            $_SESSION["sess_vLastName"] = $db_login[0]['vLastName'];
                            $_SESSION["sess_eUserType"] = $db_login[0]['eUserType'];
                            $_SESSION["sess_vContactNo"] = $db_login[0]['vContactNo'];
                            $_SESSION["sess_iGroupId"] = $db_login[0]['iGroupId'];
                            $_SESSION["sess_vUserEmail"] = $email;
                          
                            if($_SESSION["sess_iGroupId"] == 3) {
                                header('location: billing-officer/billing-query'); exit;
                            }else if ($_SESSION["sess_iGroupId"] == 1){
                                header('location: admin/query'); 
                            }else {
                                header("location: login");
                            }
                        }
                    } else {
                        $errorMsg = "User Not Found";
                    }

                }else {
                    $errorMsg = 'reCAPTHCA verification failed, please try again.';
                }
            }
            if(isset($_GET['logout']) && $_GET['logout'] == true)
            {
                session_destroy();
                header("location: portal/login");
                exit;
            }
            if(isset($_GET['lmsg']) && $_GET['lmsg'] == true)
            {
                $errorMsg = "Login required to access dashboard";
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>EcoCash Holdings - Leading PAN Africa Technology solutions group</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <style>
        .btn-primary {
    color: #fff;
    background-color: #888888;
    border-color: #888888;
    
}

body {
    background: url("./images/stock/wallet.jfif") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

    </style>
</head>
<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="porta/login" class="logo-link">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                    <img class="logo-dark logo-img logo-img-md "src="./images/wallet.jfif" srcset="./images/wallet.jfif" alt="logo-dark">
                                 
                                    </div>
                                </div>
                                <div class="nk-block-des">
                                    <?php 
                                    if(isset($errorMsg))
                                    {
                                    echo '<span style ="margin-left: -1%;"class="alert alert-danger">';
                                        echo $errorMsg;
                                    echo '</span>';
                                    unset($errorMsg);
                                    }
                                ?>       
                                </div>
                                <br/>
                                <form action="portal/login" method="post">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control form-control-lg" id="default-01" name ="vEmail" placeholder="Enter your email address" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">Password</label>
                                            <!-- <a class="link link-primary link-sm" href="forgot-password">Forgot Password?</a> -->
                                        </div>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control form-control-lg" id="password" placeholder="Enter your password" name="vPassword" required>
                                        </div><br/>
                                        <div class="g-recaptcha" data-sitekey="6LeUkr8ZAAAAAMC0ZmiMkM2tRZ_1sucyFY1WV_71"></div>
                                    </div>
                                    <div class="form-group">
                                    <button type="submit"  class="btn btn-lg btn-primary btn-block" name="login" >Sign in</button>
                                    </div>
                                </form>
                                <!-- <div class="form-note-s2 text-center pt-4"> New on our platform? <a href="portal/register">Create an account</a> -->
                                </div>
                                <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-4">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="nk-footer nk-auth-footer-full">
                        <div class="container wide-lg">
                            <div class="row g-3">
                                <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="terms-conditions">Terms & Conditions</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li> -->
                                   
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                <div class="nk-footer-copyright"> &copy; Sendr 2022 <a href="#" target="_blank">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="assets/js/bundle.js"></script>
    <script src="assets/js/scripts.js"></script>
    <!-- Google reCaptcha -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>

</html>