<?php
  
  include_once('../utils/MultiCashUtility.php');
    if(isset($_POST['reset_link']))
    {
        if(!empty($_POST['email']))
        {
            $email 		 = trim($_POST['email']);
            
            $forgotPassword = json_decode(forgotPassword($email),true);
            // var_dump($forgotPassword);
            // exit;
            if($forgotPassword['StatusCode'] == "1"){
                header("location: admin-reset-password");
                exit;
          
                       
            } else {
               
                $errorMsg = $forgotPassword['Message'];
            }
        }
    }
   

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>Mars</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <style>
        .btn-primary {
    color: #fff;
    background-color: #888888;
    border-color: #888888;
}
body {
    background: url("./images/stock/mars_home.jpeg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="html/index.html" class="logo-link">
                            <!-- <img class="logo-light logo-img logo-img-md" src="./images/ecocash_logo.png" srcset="./images/ecocash_logo.png" alt="logo"> -->
                            <img class="logo-dark logo-img logo-img-md" src="./images/mars_logo.jpeg" srcset="./images/mars_logo.jpeg" alt="logo-dark">
                                </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Reset password</h5>
                                        <div class="nk-block-des">
                                            <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
                                        </div>
                                    </div>
                                    <?php

                                    if(isset($errorMsg))

                                    {

                                    echo '<span style ="margin-left: -1%;"class="alert alert-danger">';

                                    echo $errorMsg;

                                    echo '</span>';

                                    unset($errorMsg);

                                    }

                                    ?>
                                </div>
                                <form action="portal/admin-forgot-password" method="post">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control form-control-lg" name="email" id="default-01" placeholder="Enter your email address" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <button type="submit"  class="btn btn-lg btn-primary btn-block" name="reset_link" > Send Reset Link</button>
                                    </div>
                                </form>
                                <div class="form-note-s2 text-center pt-4">
                                    <a href="portal/admin-login"><strong>Return to login</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nk-footer nk-auth-footer-full">
                        <div class="container wide-lg">
                            <div class="row g-3">
                           
                                <div class="col-lg-6">
                                    <div class="nk-block-content text-center text-lg-left">
                                    <p>&copy; 2022 VAYA TECHNOLOGIES. All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.9.0"></script>
    <script src="./assets/js/scripts.js?ver=2.9.0"></script>

</html>