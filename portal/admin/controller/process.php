<?php
include_once('../../../utils/MultiCashUtility.php');
//Query Type


//add Query Type
if (isset($_POST['add_query_type'])) {
    $queryType = $_POST['queryType'];
    $description = $_POST['description'];
    $queryTypeStatus =  $_POST['queryTypeStatus'];
    $maker = $_POST['maker'];


    $add_query_type_result =addQueryType($description,$maker,$queryType,$queryTypeStatus);
    $add_query_type_data = json_decode($add_query_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_query_type_data);
    exit;
}

//edit Query Type
if (isset($_POST['edit_query_type'])) {
    $id = $_POST['id'];
    $description = $_POST['description'];
    $queryType = $_POST['queryType'];
    $queryTypeStatus = $_POST['queryTypeStatus'];
   
    

    // var_dump($id);
    // var_dump($description);
    // var_dump($queryType);
    // var_dump($queryTypeStatus);
    // exit;

    $edit_query_type_result = editQueryType($id,$description,$queryType,$queryTypeStatus);
    $edit_query_type_data = json_decode($edit_query_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_query_type_data);
    exit;
}



    
//add currency
if (isset($_POST['add_currency'])) {
    $ccy = $_POST['ccy'];
    $description = $_POST['description'];
    $maker = $_POST['maker'];
    $status = $_POST['status'];

    
    $add_currency_result = addCurrency($ccy,$description,$maker,$status);
    $add_currency_data = json_decode($add_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_currency_data);
    exit;
}


//edit currency
if (isset($_POST['edit_currency'])) {
    $id = $_POST['id'];
    $c_type = $_POST['c_type'];
    $description = $_POST['description'];
    $checker = $_POST['checker'];
    $status = $_POST['status'];

//     var_dump($id);
//     var_dump($c_type);
//     var_dump($description);
//    var_dump($checker);
//     var_dump($status);
//     exit;
    $edit_currency_result = editCurrency($id,$c_type,$description,$checker,$status);
    $edit_currency_data = json_decode($edit_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_currency_data);
    exit;
}


//Add deduction
if (isset($_POST['add_deduction'])) {
   
    $value = $_POST['value'];
    $trip_type = $_POST['trip_type'];
    $maker = $_POST['maker'];
   

    
    // var_dump($value);
    // var_dump($trip_type);
   // var_dump($maker);
    
    // exit;
    $add_deduction_result = AddDeduction($value,$trip_type,$maker);
    $add_deduction_data = json_decode($add_deduction_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_deduction_data);
    exit;
}
//Upload Proof Of  Payment 
// if (isset($_POST['proof_of_payment'])) {
//     $partnerId = $_POST['partnerId'];
//     $queryNum = $_POST['queryNum'];
   
//     $file_name = $_FILES['myfile']['name'];
//     $filedata = $_FILES['myfile']['tmp_name'];
//     $filesize = $_FILES['myfile']['size'];
//     $filetype = $_FILES['myfile']['type'];
//     $expiry_date = date('Y-m-d');

//     var_dump($queryNum);
//     var_dump($partnerId);
//     var_dump($file_name);
//     var_dump($filetype);
//     var_dump($expiry_date);
//     var_dump($filedata);
    
//     exit;
  
//     $proof_of_payment_result =  uploadProofOfPayment($partnerId,$queryNum,$filedata,$filetype,$file_name);
//     $proof_of_payment_data = json_decode($proof_of_payment_result, true, JSON_UNESCAPED_SLASHES);
//     echo json_encode($proof_of_payment_data);
//     exit;
// }

//Config API
if (isset($_POST['add_config'])) {
    $ccy = $_POST['ccy'];
    $name = $_POST['name'];
    $value = $_POST['value'];
    $marker = $_POST['marker'];
// var_dump($ccy);
// var_dump($name);
// var_dump($value);
// var_dump($marker);
// exit;
    
    $add_config_result = addConfig($ccy, $name, $value, $marker);
    $add_config_data = json_decode($add_config_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_config_data);
    exit;
}


//PAYMENT MODE
  
//add payment mode
if (isset($_POST['add_payment_mode'])) {
    $maker = $_POST['maker'];
    $name = $_POST['name'];
    $value = $_POST['value'];
   
    
    $add_payment_result = addPaymentMode($maker,$name,$value);
    $add_payment_data = json_decode($add_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_payment_data);
    exit;
}


//edit payment mode
if (isset($_POST['edit_payment_mode'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $status = $_POST['status'];
    $value = $_POST['value'];
    $checker = $_POST['checker'];

    // var_dump($id);
    // var_dump($name);
    // var_dump($value);
    // var_dump($status);
    // var_dump($checker);
    // exit;
    $edit_payment_result =  editPaymentMode($id,$name,$value,$checker,$status);
    $edit_payment_data = json_decode($edit_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_payment_data);
    exit;
}

//Transaction Type API
  
//add Transaction Type
if (isset($_POST['add_trans_type'])) {
    $description = $_POST['description'];
    $maker = $_POST['maker'];
    $status = $_POST['status'];
    $ttype = $_POST['ttype'];
    
    $add_trans_result = addTransactionType($description,$maker,$status,$ttype);
    $add_trans_data = json_decode($add_trans_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_trans_data);
    exit;
}

//edit transaction type
if (isset($_POST['edit_transaction_type'])) {
    $id = $_POST['id'];
    $checker = $_POST['checker'];
    $description = $_POST['description'];
    $status = $_POST['status'];
    $ttype = $_POST['ttype'];

    // var_dump($id);
    // var_dump($checker);
    // var_dump($description);
    // var_dump($status);
     // var_dump($ttype);
    // exit;
    $edit_trans_result =  editTransactionType($id,$description,$status,$ttype,$checker);
    $edit_trans_data = json_decode($edit_trans_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_trans_data);
    exit;
}

//assign query
if (isset($_POST['assign_query'])) {
    $id = $_POST['id'];
    $assigned_to = $_POST['assigned_to'];
    

    // var_dump($id);
    // var_dump($assigned_to);
    // exit;
    $assign_result = AssignQuery($id,$assigned_to);
    $assign_data = json_decode($assign_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($assign_data);
    exit;
}

//verify query
if (isset($_POST['verify_query'])) {
    $id = $_POST['id'];
    $accNumber = $_POST['accNumber'];
    $assignedTo = $_POST['assignedTo'];
    $closedBy = $_POST['closedBy'];
    $distance = $_POST['distance'];
    $drAccount = $_POST['drAccount'];
    $partnerId = $_POST['partnerId'];
    $payment_mode = $_POST['payment_mode'];
    $queryNum = $_POST['queryNum'];
    $queryStatus = 'VERIFIED';
    $reject_reason = $_POST['reject_reason'];
    $tripId = $_POST['tripId'];
   
//     var_dump($id);
//     var_dump($accNumber);
//     var_dump($assignedTo);
//     var_dump($closedBy);
//     var_dump($distance);
//     var_dump($drAccount);
//     var_dump($partnerId);
//     var_dump($payment_mode);
//     var_dump($queryNum);
//     var_dump($queryStatus);
//    var_dump($reject_reason);
//     var_dump($tripId);
//     exit;

    $verify_result =  verifyQuery($id,$accNumber,$assignedTo,$closedBy,$distance,$drAccount,$partnerId,$payment_mode,$queryNum,$queryStatus,$reject_reason,$tripId);
    $verify_data = json_decode($verify_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($verify_data);
    exit;
}


//edit wallet
if (isset($_POST['edit_wallet'])) {
    $id = $_POST['id'];
    $accStatus = $_POST['accStatus'];
    $checker = $_POST['checker'];
    $taxStatus = $_POST['taxStatus'];
    

//     var_dump($id);
//     var_dump($accStatus);
//     var_dump($checker);
//    var_dump($taxStatus);
//     exit;
    $edit_wallet_result =  editWallet($id,$accStatus,$checker,$taxStatus);
    $edit_wallet_data = json_decode($edit_wallet_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_wallet_data);
    exit;
}

