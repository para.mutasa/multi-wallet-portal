<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-menu-trigger">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
        </div>
        <div class="nk-sidebar-brand">
            <a href="portal/admin-sales/products" class="logo-link nk-sidebar-logo">
                <h4 style="color:#fff;"> <span class="nk-menu-text">Multi-Cash-PORTAL</span></h4>

            </a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element nk-sidebar-body">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                            <span class="nk-menu-text">Query</span>
                        </a>
                        <ul class="nk-menu-sub">
                        <li class="nk-menu-item">
                                <a href="portal/admin/query" class="nk-menu-link"><span class="nk-menu-text">Queries</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/query-type" class="nk-menu-link"><span class="nk-menu-text">Query Types</span></a>
                            </li>
                           
                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                            <span class="nk-menu-text">Billing Officer</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/billing-officer" class="nk-menu-link"><span class="nk-menu-text">Billing Oficers</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                            <span class="nk-menu-text">Currency</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/currency" class="nk-menu-link"><span class="nk-menu-text">All Currency</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                            <span class="nk-menu-text">Payments</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/payment-mode" class="nk-menu-link"><span class="nk-menu-text">Payment Mode</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                            <span class="nk-menu-text">Transactions</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/transaction-type" class="nk-menu-link"><span class="nk-menu-text">Transaction Type</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                            <span class="nk-menu-text">Deductions</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/deductions" class="nk-menu-link"><span class="nk-menu-text">Deductions</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                            <span class="nk-menu-text">Wallets</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/wallet" class="nk-menu-link"><span class="nk-menu-text">Wallets</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                   

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-chat-circle-fill"></em></span>
                            <span class="nk-menu-text">Configs</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                                <a href="portal/admin/configs" class="nk-menu-link"><span class="nk-menu-text">All Configs</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-reports"></em></span>
                            <span class="nk-menu-text">Reports</span>
                        </a>
                        <ul class="nk-menu-sub">
                        <li class="nk-menu-item">
                                <a href="portal/admin/partner-wallet" class="nk-menu-link"><span class="nk-menu-text">Partner Wallet Statement</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/all-query-report" class="nk-menu-link"><span class="nk-menu-text">All Queries</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/wallet-withdrawal-report" class="nk-menu-link"><span class="nk-menu-text">Wallet Withdrawal</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/instant-tansfers-report" class="nk-menu-link"><span class="nk-menu-text">Instant Transfers</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/deposit-report" class="nk-menu-link"><span class="nk-menu-text">All Deposits</span></a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="portal/admin/vaya-commission" class="nk-menu-link"><span class="nk-menu-text">Vaya Commission</span></a>
                            </li>

                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->


                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>