<?php
session_start();
if (!isset($_SESSION['sess_iGroupId'])) {
    header("Location: ../login");
    exit();
}
$iAdminId = $_SESSION['sess_iAdminId'];
$vFirstName = $_SESSION["sess_vFirstName"];
$vlastName = $_SESSION["sess_vLastName"];
$vContactNo  = $_SESSION["sess_vContactNo"];
$iGroupId = $_SESSION["sess_iGroupId"];
$email = $_SESSION["sess_vUserEmail"];

include_once('../../utils/MultiCashUtility.php');
require_once('includes/header.php');
require_once('includes/header.php');
$currencies = json_decode(getAllCurrency(), true);
//$payments = json_decode(getAllPaymentModes(), true);
if (isset($_POST['data_filter'])) {

    $ccy     = $_POST['ccy'];
    $paymentMode    = $_POST['paymentMode'];
    $commissions = json_decode(VayaCommission($ccy, $paymentMode), true);
}
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $vFirstName; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $vlastName; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="portal/admin/user-profile-regular"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="portal/admin/user-profile-setting"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="portal/admin/user-profile-activity"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title"> Vaya Commission Report</h3>

                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <!-- <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="portal/partner/add-booking"><span>Add Booking</span></a></li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    <form method="post" action="portal/admin/vaya-commission">
                                        <div class="row">

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-label">Currency</label>
                                                    <div class="form-control-wrap">
                                                        <select id="dropdown1" name="ccy" class="form-select" data-placeholder="--All--" required>
                                                            <?php foreach ($currencies['data']  as $currency) : ?>

                                                                <option value="<?= $currency["ccy"] ?>"><?= $currency["ccy"] . "" ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-label">Payment Mode</label>
                                                    <div class="form-control-wrap">
                                                        <select class="form-select" name="paymentMode" id="dropdown2" data-placeholder="Select payment Mode ">
                                                            <option value="">--All--</option>
                                                            <option value="CODE">CODE</option>
                                                            <option value="CASH">CASH</option>
                                                            <option value="ECOCASH">ECOCASH</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="form-label"></label>
                                                    <div class="form-control-wrap">
                                                        <button class="btn btn-primary" type="submit" name="data_filter">Filter</button>
                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </form>
                                    </br>
                                    <div class="card card-preview">
                                        <div class="card-inner">
                                            <table class="datatable-init-export table nowrap nk-tb-list nk-tb-ulist" data-order='[[2, "desc"]]' id="responses" data-auto-responsive="false">
                                                <thead>
                                                    <tr class="nk-tb-item nk-tb-head">
                                                    <th class="nk-tb-col"><span class="sub-text">Trip Id</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">trnRefNo</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Transaction Date</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">trnCode</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">trnAmount</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Currency</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">New Balance</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Debit Account</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Credit Account</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Payment Mode</span></th>


                                                </thead>
                                                <tbody>
                                                    <?php if ($commissions['status'] ==  "SUCCESS") : ?>

                                                        <?php
                                                        foreach ($commissions['data'] as $commission) :
                                                        ?>
                                                            <tr>
                                                            <td class="nk-tb-col tb-col-md"><span><?= $commission['tripId'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['trnRefNo'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['transactionDate'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['trnCode'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['trnAmount'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['ccy'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['newBalance'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['drAccount'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['crAccount'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $commission['paymentMode'] ?></span></td>



                                                            <?php endforeach; ?>

                                                        <?php else : ?>

                                                            <?= $commissions['message'] ?>


                                                        <?php endif; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!-- .card-preview -->
                                </div> <!-- nk-block -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully re-assigned driver</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='assignDriverResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScript -->
    <?php require_once('includes/footer.php'); ?>

    <!--Start Datatable filter -->
    <script>
        $(document).ready(function() {
            var table = $('#responses').DataTable();

            $('#dropdown1').on('change', function() {
                table.search(this.value).draw();
            });
            $('#dropdown2').on('change', function() {
                table.search(this.value).draw();
            });

            $('#datefilterfrom').on('change', function() {
                table.columns(2).search(this.value).draw();
            });

            $('#datefilterto').on('change', function() {
                table.columns(2).search(this.value).draw();
            });
        });
    </script>

    <script>
        $('#datefilterfrom').datepicker({
            format: 'yyyy-mm-dd',
            clearBtn: true
        });
        $('#datefilterto').datepicker({
            format: 'yyyy-mm-dd',
            clearBtn: true
        });
    </script>
    <script>
        $(function() {
            $("#datefilterto").datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#datefilterfrom").datepicker({
                dateFormat: 'yy-mm-dd'
            }).bind("change", function() {
                var minValue = $(this).val();
                minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
                minValue.setDate(minValue.getDate() + 1);
                $("#to").datepicker("option", "minDate", minValue);
            })
        });
    </script>
    <!--End Datatable filter -->

    <script>
        $(document).ready(function() {
            $(".export").click(function() {
                var export_type = $(this).data('export-type');
                $('#example').tableExport({
                    type: export_type,
                    escape: 'false',
                    ignoreColumn: []
                });
            });
        });
    </script>



</body>

</html>