<?php
session_start();
if (!isset($_SESSION['sess_iGroupId'])) {
    header("Location: ../login");
    exit();
}
$iAdminId = $_SESSION['sess_iAdminId'];
$vFirstName = $_SESSION["sess_vFirstName"];
$vlastName = $_SESSION["sess_vLastName"];
$vContactNo  = $_SESSION["sess_vContactNo"];
$iGroupId = $_SESSION["sess_iGroupId"];
$email = $_SESSION["sess_vUserEmail"];

include_once('../../utils/MultiCashUtility.php');
require_once('includes/header.php');
$currencies = json_decode(getAllCurrency(), true);
//$configs= json_decode(getAllConfigs(), true);

// var_dump($currencies);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $vFirstName; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $vlastName; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Currency List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt"><a href="#" data-toggle="modal" data-target="#add-currency" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Currency</span></a></li>

                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">

                                    <div class="card card-preview">
                                        <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-order='[[0, "desc"]]' data-auto-responsive="false">
                                                <thead>
                                                    <tr class="nk-tb-item nk-tb-head">
                                                        <th class="nk-tb-col"><span class="sub-text">Currency Name</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Description</span></th>

                                                        <th class="nk-tb-col nk-tb-col-tools text-right">
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($currencies['status'] ==  "SUCCESS") : ?>
                                                        <?php
                                                        foreach ($currencies['data'] as $currency) :
                                                        ?>
                                                            <tr>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $currency['ccy'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $currency['description'] ?></span></td>

                                                                <td class="nk-tb-col nk-tb-col-tools">
                                                                    <ul class="nk-tb-actions gx-1">

                                                                        <li>
                                                                            <div class="drodown">
                                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                                    <ul class="link-list-opt no-bdr">
                                                                                        <li><a href="#" data-toggle="modal" data-target="#edit-currency<?= $currency['id'] ?>"><em class="icon ni ni-edit"></em><span>Edit Currency</span></a></li>

                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <!-- update Currency-->
                                                                <div class="modal fade" tabindex="-1" role="dialog" id="edit-currency<?= $currency['id'] ?>">
                                                                    <div class="modal-dialog modal-md" role="document">
                                                                        <div class="modal-content">
                                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                            <div class="modal-body modal-body-lg">
                                                                                <div class="gy-4">
                                                                                    <div class="example-alert">
                                                                                        <div class="alert alert-pro alert-primary">
                                                                                            <div class="alert-text">
                                                                                                <h5 class="modal-title">Edit Currency</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <form id="edit-currency<?= $currency['id'] ?>" class="mt-2">
                                                                                    <div class="row g-gs">
                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="payment-name-add">Name</label>
                                                                                                <input type="text" class="form-control" name="c_type" value="<?= $currency['ccy'] ?>">
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label"> Status</label>
                                                                                                <div class="form-control-wrap">
                                                                                                    <select name="status" class="form-select" data-placeholder="Select Staus" value="<?= $currency['status'] ?>">
                                                                                                        <option value="A">ACTIVE</option>
                                                                                                        <option value="D">DELETED</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-12">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="payment-name-add">Description</label>
                                                                                                <input type="text" class="form-control" name="description" value="<?= $currency['description'] ?>">
                                                                                            </div>
                                                                                        </div>




                                                                                        <input type="hidden" class="form-control" name="checker" value="<?php echo $email; ?>">
                                                                                        <input type="hidden" class="form-control" name="id" value="<?= $currency['id'] ?>">

                                                                                        <div class="col-12">
                                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                <li>
                                                                                                    <input type="hidden" name="edit_currency" value="true">
                                                                                                    <button type="button" class="btn btn-primary" name="edit_currency" onClick="editCurrency('<?= $currency["id"] ?>')">Submit</button>
                                                                                                </li>

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div><!-- .modal-body -->
                                                                            <div class="modal-footer bg-light">
                                                                                <span class="sub-text"></span>
                                                                            </div>
                                                                        </div><!-- .modal-content -->
                                                                    </div><!-- .modal-dialog -->
                                                                </div><!-- .modal -->


                                                            <?php endforeach; ?>

                                                        <?php else : ?>

                                                            <?= $currencies['message'] ?>

                                                        <?php endif; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!-- .card-preview -->
                                </div> <!-- nk-block -->


                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successCurrencyAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully updated currency</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failCurrencyAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='updateCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully added currency</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='addCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully updated currency</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='editCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Currency-->
    <div class="modal fade" tabindex="-1" role="dialog" id="add-currency">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <div class="gy-4">
                        <div class="example-alert">
                            <div class="alert alert-pro alert-primary">
                                <div class="alert-text">
                                    <h5 class="modal-title">Add Currency</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status">    <span class="sr-only">Loading...</span>  </div></div> -->
                    <form id="add-currency" action="#" class="mt-2">
                        <div class="row g-gs">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Currency Name</label>
                                    <input type="text" class="form-control" name="ccy" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="payment-name-add">Description</label>
                                    <input type="text" class="form-control" name="description" placeholder="">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Status </label>
                                    <div class="form-control-wrap">
                                        <select name="status" class="form-select" data-placeholder="Select Status">
                                            <option value="A">ACTIVE</option>
                                            <option value="D">DELETED</option>


                                        </select>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" class="form-control" name="maker" value="<?php echo $email; ?>">
                            <input type="hidden" class="form-control" name="id" value="<?= $currency['id'] ?>">
                            <div class="col-10">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <input type="hidden" class="form-control" name="add_currency">
                                        <button type="button" class="btn btn-primary" name="add_currency" onClick="addCurrency()">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->

    <!-- BEGIN: AJAX CALLS-->
    <script>
        //Add Currency
        function addCurrency() {
            $.ajax({
                type: "POST",
                url: "portal/admin/controller/process.php",
                data: $('form#add-currency').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.status == "SUCCESS") {
                        $('#add-currency').modal('hide');
                        $("#successAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/admin/currency";
                        }, 2000);
                    } else {
                        $('.addCurrencyResponse').empty();
                        $('.addCurrencyResponse').html(json.message);
                        $('#add-currency').modal('hide');
                        $("#failAlert").modal('show');
                        $("#failAlert").on("hidden.bs.modal", function() {
                            $(".addCurrencyResponse").html("");
                        });

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.addCurrencyResponse').empty();
                    $('.addCurrencyResponse').append(errorThrown);
                    $('#add-currency').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addCurrencyResponse").html("");
                    });
                }
            });
        }
    </script>

    <script>
        //edit currency
        function editCurrency(id) {
            $.ajax({
                type: "POST",
                url: "portal/admin/controller/process.php",
                data: $('form#edit-currency' + id).serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.status == "SUCCESS") {
                        $('#edit-currency' + id).modal('hide');
                        $("#successCurrencyAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/admin/currency";
                        }, 2000);

                    } else {
                        $('.updateCurrencyResponse').empty();
                        $('.updateCurrencyResponse').append(json.message);
                        $('#edit-currency' + id).modal('hide');
                        $("#failCurrencyAlert").modal('show');
                        $("#failCurrencyAlert").on("hidden.bs.modal", function() {
                            $(".updateCurrencyResponse").html("");
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.updateCurrencyResponse').empty();
                    $('.updateCurrencyResponse').append(errorThrown);
                    $('#edit-currency' + id).modal('hide');
                    $("#failCurrencyAlert").modal('show');
                    $("#failCurrencyAlert").on("hidden.bs.modal", function() {
                        $(".updateCurrencyResponse").html("");
                    });
                }
            });
        }
    </script>

    <!-- JavaScript -->
    <?php require_once('includes/footer.php'); ?>
</body>

</html>