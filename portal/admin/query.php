<?php
session_start();
if (!isset($_SESSION['sess_iGroupId'])) {
    header("Location: ../login");
    exit();
}
$iAdminId = $_SESSION['sess_iAdminId'];
$vFirstName = $_SESSION["sess_vFirstName"];
$vlastName = $_SESSION["sess_vLastName"];
$vContactNo  = $_SESSION["sess_vContactNo"];
$iGroupId = $_SESSION["sess_iGroupId"];

$email = $_SESSION["sess_vUserEmail"];
include_once('../../utils/MultiCashUtility.php');
require_once('includes/header.php');




$page               = !empty($_GET['page']) ? (int)$_GET['page'] : 0;
$items_per_page     = 20;
$items_total_count  = json_decode(CountQueries(),true)['data'];
$paginate           = new Paginate($page, $items_per_page, $items_total_count);
$queries        = json_decode(getAllQueries($page,$items_per_page ), true);

$officers = json_decode(getAllBillingOfficers(), true);
$query_types = json_decode(CountQuery('Overstated'), true)['data']['totalNumber'];
$query_Rtypes = json_decode(RCountQuery('Reversal'), true)['data']['totalNumber'];

?>

<body class="nk-body bg-lighter npc-general has-sidebar" onload="countQuery()">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $vFirstName; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $vlastName; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Queries List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <!-- <li class="nk-block-tools-opt"><a href="#"  data-toggle="modal" data-target="#add-currency" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Currency</span></a></li> -->

                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">Datepicker Range Filter</label>
                                                    <div class="form-control-wrap">
                                                        <div class="input-daterange date-picker-range input-group">
                                                            <input type="text" id="datefilterfrom" class="form-control" />
                                                            <div class="input-group-addon">TO</div>
                                                            <input type="text" id="datefilterto" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->



                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-label">Query Type</label>
                                                    <div class="form-control-wrap">
                                                        <select class="form-select" id="dropdown2" data-placeholder="Select Query Type">
                                                            <option value="">--All--</option>
                                                            <option value="Overstated">Overstated</option>
                                                            <option value="Reversal">Wallet Credit</option>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </br>

                                        <div class="card card-preview">
                                            <div class="card-inner">
                                                <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" id="query_list" data-order='[[0, "desc"]]' data-paging='false' data-Info='false' data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            
                                                            <th class="nk-tb-col"><span class="sub-text">Query Number</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Distance</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">partner Id</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">query Type</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Payment Mode</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Amount</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Query Status</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Created Date</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Closed By</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Close Date</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if ($queries['status'] ==  "SUCCESS") : ?>
                                                            <?php
                                                            foreach ($queries['data']['content'] as $query) :
                                                            ?>
                                                                <tr>
                                                                    
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['queryNum'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['distance'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['partnerId'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['queryType'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['payment_mode'] ?></span></td> 
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['amount'] ?></span></td>                         
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['queryStatus'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['createdDate'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['closedBy'] ?></span></td>
                                                                    <td class="nk-tb-col tb-col-md"><span><?= $query['closeDate'] ?></span></td>
                                                                    <td class="nk-tb-col nk-tb-col-tools">
                                                                        <ul class="nk-tb-actions gx-1">

                                                                            <li>
                                                                                <div class="drodown">
                                                                                    <a class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                                        <ul class="link-list-opt no-bdr">
                                                                                        <?php
                                                                                            if($query['queryType'] == "Overstated"){
                                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#assign-query".$query['id']."' ><em class='icon ni ni-edit'></em><span>Assign Query</span></a></li>";
                                                                                                echo"<li><a  data-toggle='modal' data-target='#verify-query".$query['id']."' ><em class='icon ni ni-edit'></em><span>Verify Query</span></a></li>";
                                                                                            }else {                                     

                                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#assign-query".$query['id']."' ><em class='icon ni ni-edit'></em><span>Assign Query</span></a></li>";
                                                                                            }  
                                                                                            ?>

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                    <!-- assign query-->
                                                                    <div class="modal fade" tabindex="-1" role="dialog" id="assign-query<?= $query['id'] ?>">
                                                                        <div class="modal-dialog modal-md" role="document">
                                                                            <div class="modal-content">
                                                                                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                                <div class="modal-body modal-body-lg">
                                                                                    <div class="gy-4">
                                                                                        <div class="example-alert">
                                                                                            <div class="alert alert-pro alert-primary">
                                                                                                <div class="alert-text">
                                                                                                    <h5 class="modal-title">Assign Query to Billing Officer</h5>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <form id="assign-query<?= $query['id'] ?>" class="mt-2">
                                                                                        <div class="row g-gs">
                                                                                            <div class="col-md-10">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label"> Billing Officer</label>
                                                                                                    <div class="form-control-wrap">
                                                                                                        <select class="form-control" name="assigned_to" required>
                                                                                                            <?php foreach ($officers['data']  as $officer) : ?>

                                                                                                                <option value="<?= $officer["email"] ?>"><?= $officer["fullName"] . "" ?></option>
                                                                                                            <?php endforeach; ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input type="hidden" class="form-control" name="id" value="<?= $query['id'] ?>">

                                                                                            <div class="col-12">
                                                                                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                    <li>
                                                                                                        <input type="hidden" name="assign_query" value="true">
                                                                                                        <button type="button" class="btn btn-primary" name="assign_query" onClick="AssignQuery('<?= $query["id"] ?>')">Submit</button>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div><!-- .modal-body -->
                                                                                <div class="modal-footer bg-light">
                                                                                    <span class="sub-text"></span>
                                                                                </div>
                                                                            </div><!-- .modal-content -->
                                                                        </div><!-- .modal-dialog -->
                                                                    </div><!-- .modal -->
                                                                    <!-- verify query-->
                                                                    <div class="modal fade" tabindex="-1" role="dialog" id="verify-query<?= $query['id'] ?>">
                                                                        <div class="modal-dialog modal-md" role="document">
                                                                            <div class="modal-content">
                                                                                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                                <div class="modal-body modal-body-lg">
                                                                                    <div class="gy-4">
                                                                                        <div class="example-alert">
                                                                                            <div class="alert alert-pro alert-primary">
                                                                                                <div class="alert-text">
                                                                                                    <h5 class="modal-title">Verify Query</h5>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <form id="verify-query<?= $query['id'] ?>" class="mt-2">
                                                                                        <div class="row g-gs">

                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Trip Id</label>
                                                                                                    <input type="text" class="form-control" name="tripId" value="<?= $query['tripId'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">partner Id </label>
                                                                                                    <input type="text" class="form-control" name="partnerId" value="<?= $query['partnerId'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Payment Mode </label>
                                                                                                    <input type="text" class="form-control" name="payment_mode" value="<?= $query['payment_mode'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Account Number </label>
                                                                                                    <input type="text" class="form-control" name="accNumber" value="<?= $query['accountNumber'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Distance</label>
                                                                                                    <input type="text" class="form-control" name="distance" value="<?= $query['distance'] ?>">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">customer</label>
                                                                                                    <input type="text" class="form-control" name="customer" value="<?= $query['customer'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Pick Up Point</label>
                                                                                                    <input type="text" class="form-control" name="pickupPoint" value="<?= $query['pickupPoint'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Connection Point</label>
                                                                                                    <input type="text" class="form-control" name="connectionPoint" value="<?= $query['connectionPoint'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Destination</label>
                                                                                                    <input type="text" class="form-control" name="destination" value="<?= $query['destination'] ?>" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                                                    <label class="form-label" for="payment-name-add">Supporting Document</label>
                                                                                                    <?php
                                                                                                    if ($query['attachment'] == "") {
                                                                                                        echo "UNAVAILABLE";
                                                                                                        //     echo "<li><a  data-toggle='modal' data-target='#support-doc" . $query['id'] . "' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";
                                                                                                    } else {
                                                                                                        echo "<a target='_blank' href='" . $query['attachment'] . "' class='btn btn-dim btn-sm btn-primary' title='Supporting Document '>View</a>";
                                                                                                    }
                                                                                                    ?>
                                                                                                </div>
                                                                                            </div>



                                                                                            <input type="hidden" class="form-control" name="closedBy" value="<?php echo $admin_email; ?>">
                                                                                            <input type="hidden" class="form-control" name="id" value="<?= $query['id'] ?>">







                                                                                            <div class="col-12">
                                                                                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                    <li>
                                                                                                        <input type="hidden" name="verify_query" value="true">
                                                                                                        <button type="button" class="btn btn-primary" name="verify_query" onClick="verifyQuery('<?= $query["id"] ?>')">Submit</button>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div><!-- .modal-body -->
                                                                                <div class="modal-footer bg-light">
                                                                                    <span class="sub-text"></span>
                                                                                </div>
                                                                            </div><!-- .modal-content -->
                                                                        </div><!-- .modal-dialog -->
                                                                    </div><!-- .modal -->


                                                                <?php endforeach; ?>

                                                            <?php else : ?>

                                                                <?= $queries['message'] ?>

                                                            <?php endif; ?>


                                                    </tbody>
                                                </table>
                                                <div class="col-sm-4" style="padding: 0;">
                                                <p>Showing <?= $paginate->offset() + 1 ?> to <?= ($paginate->offset() + 20) > $items_total_count ? $items_total_count : $paginate->offset() + 20 ?> of <?= $items_total_count ?> entries</p>
                                            </div>
                                            <div class="col-sm-8 tp-pagination" style="padding: 0;">
                                                <ul class="pagination pull-right" style="margin: 0;">
                                                    <?php if ($paginate->page_total() > 1) : ?>
                                                        <?php if ($paginate->has_previous()) : ?>
                                                            <li>
                                                                <a href="portal/admin/query?page=<?php echo $paginate->previous(); ?>" aria-label="Previous"> <span aria-hidden="true">Previous</span> </a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <?php
                                                        for ($i = 0; $i <= $paginate->page_total(); $i++) {
                                                            if ($paginate->current_page > 5 && $i == 1) {
                                                                echo "<li><a href='portal/admin/query?page={$i}'>{$i}</a></li><li><a>...</a></li>";
                                                            }
                                                            if ($i == $paginate->current_page) {
                                                                echo "<li class='active'><a href='portal/admin/query?page={$i}'>{$i}</a></li>";
                                                            } elseif ($i > ($paginate->current_page - 4) && $i < ($paginate->current_page + 4) && $i != $paginate->current_page) {
                                                                echo "<li><a href='portal/admin/query?page={$i}'>{$i}</a></li>";
                                                            }
                                                            if (($paginate->current_page + 4) < $i && $i == $paginate->page_total()) {
                                                                echo "<li><a>...</a></li><li><a href='portal/admin/query?page={$i}'>{$i}</a></li>";
                                                            }
                                                        }
                                                        ?>
                                                        <?php if ($paginate->has_next()) : ?>
                                                            <li>
                                                                <a href="portal/admin/query?page=<?php echo $paginate->next(); ?>" aria-label="Next"> <span aria-hidden="true">NEXT</span> </a>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!-- .card-preview -->
                                </div> <!-- nk-block -->


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content @e -->
                    <!-- footer @s -->
                    <div class="nk-footer">
                        <div class="container-fluid">
                            <div class="nk-footer-wrap">
                                <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- footer @e -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- main @e -->
        </div>
        <!-- app-root @e -->

        <!-- Success Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successAssignAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully asigned query</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCurrencyAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='updateCurrencyResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Success Modal Alert count queries-->
        <div class="modal fade" tabindex="-1" id="countQuery">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve <? $query_types ?> Overstated Queries</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='addCurrencyResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Success Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successEditAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully assigned query to billing officer</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='editCurrencyResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Success Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successverifyEditAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully verified query</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="verifyfailEditAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='verifyResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Proof Of  Payment  Success Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successProofPaymentAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">Proof Of Payment has been uploaded successfully</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail Proof Of Payment  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failproofPaymentAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='proofPaymentResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- JavaScript -->
        <?php require_once('includes/footer.php'); ?>

        <script>
            function countQuery() {
                //  $('#countQuery').modal('show');
                var value = <?php echo $query_types ?>;
                var Rvalue = <?php echo $query_Rtypes ?>;
                alert('You have ' + value + ' Overstated Queries and '+ Rvalue + ' Reversal Queries');
            }
        </script>

        <!--Start Datatable filter -->
        <script>
            $(document).ready(function() {
                var table = $('#query_list').DataTable();

                table.column(4).data().search('Reversal').draw();


                $('#dropdown1').on('change', function() {
                    table.search(this.value).draw();
                });
                $('#dropdown2').on('change', function() {
                    table.search(this.value).draw();
                });

                $('#datefilterfrom').on('change', function() {
                    table.columns(6).search(this.value).draw();
                });

                $('#datefilterto').on('change', function() {
                    table.columns(6).search(this.value).draw();
                });
            });
        </script>

        <script>
            $('#datefilterfrom').datepicker({
                format: 'dd-mm-yyyy',
                clearBtn: true
            });
            $('#datefilterto').datepicker({
                format: 'dd-mm-yyyy',
                clearBtn: true
            });
        </script>
        <script>
            $(function() {
                $("#datefilterto").datepicker({
                    dateFormat: 'dd-mm-yy'
                });
                $("#datefilterfrom").datepicker({
                    dateFormat: 'dd-mm-yy'
                }).bind("change", function() {
                    var minValue = $(this).val();
                    minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
                    minValue.setDate(minValue.getDate() + 1);
                    $("#to").datepicker("option", "minDate", minValue);
                })
            });
        </script>
        <!--End Datatable filter -->

        <script>
            function reject_reason() {
                document.getElementById('hide_all').style.display = 'none';
            }

            function getValue() {
                const user_value = document.getElementById("user_value").value;
                console.log(user_value);
                if (user_value == "REJECTED") {
                    document.getElementById('hide_all').style.display = 'inline-block';


                } else if (user_value == "") {
                    document.getElementById('hide_all').style.display = 'none';
                } else {
                    document.getElementById('hide_all').style.display = 'none';
                }
            }
        </script>

        <!--Start Datatable filter -->
        <script>
            $(document).ready(function() {
                var table = $('#query_list').DataTable();

                table.column(3).data().search('Reversal').draw();


                $('#dropdown1').on('change', function() {
                    table.search(this.value).draw();
                });
                $('#dropdown2').on('change', function() {
                    table.search(this.value).draw();
                });

                $('#datefilterfrom').on('change', function() {
                    table.columns(5).search(this.value).draw();
                });

                $('#datefilterto').on('change', function() {
                    table.columns(5).search(this.value).draw();
                });
            });
        </script>

        <script>
            $('#datefilterfrom').datepicker({
                format: 'yyyy-mm-dd',
                clearBtn: true
            });
            $('#datefilterto').datepicker({
                format: 'yyyy-mm-dd',
                clearBtn: true
            });
        </script>
        <script>
            $(function() {
                $("#datefilterto").datepicker({
                    format: 'yyyy-mm-dd'
                });
                $("#datefilterfrom").datepicker({
                    dateFormat: 'yy-mm-dd'
                }).bind("change", function() {
                    var minValue = $(this).val();
                    minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
                    minValue.setDate(minValue.getDate() + 1);
                    $("#to").datepicker("option", "minDate", minValue);
                })
            });
        </script>
        <!--End Datatable filter -->

        <!-- BEGIN: AJAX CALLS-->

        <script>
            //Assign query
            function AssignQuery(id) {
                $.ajax({
                    type: "POST",
                    url: "portal/admin/controller/process.php",
                    data: $('form#assign-query' + id).serialize(),
                    cache: false,
                    success: function(response) {
                        var json = $.parseJSON(response);
                        console.log(json);
                        if (json.status == "SUCCESS") {
                            // alert("Success")
                            $('#assign-query' + id).modal('hide');
                            $("#successAssignAlert").modal('show');
                            setTimeout(function() {
                                window.location = "portal/admin/query";
                            }, 2000);

                        } else {
                            $('.updateCurrencyResponse').empty();
                            $('.updateCurrencyResponse').append(json.message);
                            $('#assign-query' + id).modal('hide');
                            $("#failCurrencyAlert").modal('show');
                            $("#failCurrencyAlert").on("hidden.bs.modal", function() {
                                $(".updateCurrencyResponse").html("");
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('.updateCurrencyResponse').empty();
                        $('.updateCurrencyResponse').append(errorThrown);
                        $('#assign-query' + id).modal('hide');
                        $("#failCurrencyAlert").modal('show');
                        $("#failCurrencyAlert").on("hidden.bs.modal", function() {
                            $(".updateCurrencyResponse").html("");
                        });
                    }
                });
            }
        </script>
        <script>
            //verify query
            function verifyQuery(id) {
                $.ajax({
                    type: "POST",
                    url: "portal/admin/controller/process.php",
                    data: $('form#verify-query' + id).serialize(),
                    cache: false,
                    success: function(response) {
                        var json = $.parseJSON(response);
                        if (json.status == "SUCCESS") {
                            $('#verify-query' + id).modal('hide');
                            $("#successverifyEditAlert").modal('show');
                            setTimeout(function() {
                                window.location = "portal/admin/query";
                            }, 2000);

                        } else {
                            $('.verifyResponse').empty();
                            $('.verifyResponse').append(json.message);
                            $('#verify-query' + id).modal('hide');
                            $("#verifyfailEditAlert").modal('show');
                            $("#verifyfailEditAlert").on("hidden.bs.modal", function() {
                                $(".verifyResponse").html("");
                            });
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('.verifyResponse').empty();
                        $('.verifyResponse').append(errorThrown);
                        $('#verify-query' + id).modal('hide');
                        $("#verifyfailEditAlert").modal('show');
                        $("#verifyfailEditAlert").on("hidden.bs.modal", function() {
                            $(".verifyResponse").html("");
                        });
                    }
                });
            }
        </script>


</body>

</html>