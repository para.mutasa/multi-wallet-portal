<?php
include_once('../../../utils/MultiCashUtility.php');


//accept-reject query
if (isset($_POST['reject_accept_query'])) {
    $id = $_POST['id'];
    $checker = $_POST['checker'];
    $acceptRejectStatus = $_POST['acceptRejectStatus'];
    $tripId = $_POST['tripId'];
    $queryNum = $_POST['queryNum'];
    $reject_reason = $_POST['reject_reason'];

    // var_dump($id);
    // var_dump($checker);
    // var_dump($acceptRejectStatus);
    // var_dump($tripId);
    // var_dump($queryNum);
    // var_dump($reject_reason);
    // exit;
    $reject_accept_result = rejectAcceptQuery($id,$acceptRejectStatus,$queryNum,$reject_reason,$tripId,$checker);
    $reject_accept_data = json_decode($reject_accept_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($reject_accept_data);
    exit;
}


//verify tax
if (isset($_POST['verify_tax'])) {
    $id = $_POST['id'];
    $description = $_POST['description'];
    $document_status = 'ACTIVE';
    $maker = $_POST['maker'];
    $tax_clearance_number = $_POST['tax_clearance_number'];
    $verify_tax_result =  verifyTax($id,$description,$document_status,$maker,$tax_clearance_number);
    $verify_tax_data = json_decode($verify_tax_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($verify_tax_data);
    exit;

}

//verify POP
if (isset($_POST['verify_pop'])) {
    $id = $_POST['id'];
    $reason = $_POST['reason'];
    $status = $_POST['status'];
    $maker = $_POST['maker'];
    
    $verify_pop_result =  verifyPOP($id,$reason,$status,$maker);
    $verify_pop_data = json_decode($verify_pop_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($verify_pop_data);
    exit;

}
//verify withdrawal request
if (isset($_POST['verify_request'])) {
    $id = $_POST['id'];
    $addlText = $_POST['addlText'];
    $authStat = $_POST['authStat'];
    
    $verify_request_result = verifyRequest($id,$addlText,$authStat);
    $verify_request_data = json_decode($verify_request_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($verify_request_data);
    exit;

}
  
    
    ?>
