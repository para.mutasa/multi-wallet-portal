<?php
session_start();
if (!isset($_SESSION['sess_iGroupId'])) {
    header("Location: ../login");
    exit();
}
$iAdminId = $_SESSION['sess_iAdminId'];
$vFirstName = $_SESSION["sess_vFirstName"];
$vlastName = $_SESSION["sess_vLastName"];
$vContactNo  = $_SESSION["sess_vContactNo"];
$iGroupId = $_SESSION["sess_iGroupId"];
$email = $_SESSION["sess_vUserEmail"];

include_once('../../utils/MultiCashUtility.php');
require_once('includes/header.php');

$page               = !empty($_GET['page']) ? (int)$_GET['page'] : 0;
$items_per_page     = 20;
$items_total_count  = json_decode(CountPOPs(),true)['data'];
$paginate           = new Paginate($page, $items_per_page, $items_total_count);
$pops        = json_decode(getAllPOP($page, $items_per_page), true);

?>

<body class="nk-body bg-lighter npc-general has-sidebar" onload="reason()">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $vFirstName; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $vlastName; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">POP List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <!-- <li class="nk-block-tools-opt"><a href="#"  data-toggle="modal" data-target="#add-currency" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Currency</span></a></li> -->

                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">

                                    <div class="card card-preview">
                                        <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-order='[[0, "desc"]]' data-paging='false' data-Info='false' data-auto-responsive="false">
                                                <thead>
                                                    <tr class="nk-tb-item nk-tb-head">
                                                        <th class="nk-tb-col"><span class="sub-text">ID</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">drAccount</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Currency</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">Status</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">accountNumber</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">trnRefNo</span></th>
                                                        <th class="nk-tb-col"><span class="sub-text">transactionCode</span></th>

                                                        <th class="nk-tb-col nk-tb-col-tools text-right">
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($pops['status'] ==  "SUCCESS") : ?>
                                                        <?php
                                                        foreach ($pops['data']['content'] as $pop) :
                                                        ?>
                                                            <tr>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['id'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['drAccount'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['ccy'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['status'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['accountNumber'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['trnRefNo'] ?></span></td>
                                                                <td class="nk-tb-col tb-col-md"><span><?= $pop['transactionCode'] ?></span></td>

                                                                <td class="nk-tb-col nk-tb-col-tools">
                                                                    <ul class="nk-tb-actions gx-1">

                                                                        <li>
                                                                            <div class="drodown">
                                                                                <a class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                                    <ul class="link-list-opt no-bdr">


                                                                                        <li><a data-toggle="modal" data-target="#verify-pop<?= $pop['id'] ?>"><em class="icon ni ni-edit"></em><span>Verify POP</span></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </td>


                                                                <!-- verify POP-->
                                                                <div class="modal fade" tabindex="-1" role="dialog" id="verify-pop<?= $pop['id'] ?>">
                                                                    <div class="modal-dialog modal-md" role="document">
                                                                        <div class="modal-content">
                                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                                            <div class="modal-body modal-body-lg">
                                                                                <div class="gy-4">
                                                                                    <div class="example-alert">
                                                                                        <div class="alert alert-pro alert-primary">
                                                                                            <div class="alert-text">
                                                                                                <h5 class="modal-title">Verify POP</h5>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <form id="verify-pop<?= $pop['id'] ?>" class="mt-2">
                                                                                    <div class="row g-gs">

                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="payment-name-add">View POP Document</label>
                                                                                                <?php
                                                                                                if ($pop['attachment'] == "") {
                                                                                                    echo "UNAVAILABLE";
                                                                                                    //     echo "<li><a  data-toggle='modal' data-target='#support-doc" . $query['id'] . "' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";
                                                                                                } else {
                                                                                                    echo "<a target='_blank' href='" . $pop['attachment'] . "' class='btn btn-dim btn-sm btn-primary' title='POP Document '>View</a>";
                                                                                                }
                                                                                                ?>
                                                                                            </div>
                                                                                        </div>




                                                                                        <div class="col-md-6">

                                                                                            <div class="form-group">

                                                                                                <div class="form-control-wrap">

                                                                                                    <option label="" value="">Accept / Reject</option>
                                                                                                    <select class="form-control form-select" name="status" id="user_value" onChange="getValue();" data-placeholder="Select an option" required>
                                                                                                        <option label="" value="">Select an option</option>
                                                                                                        <option value="REJECTED">REJECTED</option>
                                                                                                        <option value="ACCEPTED">ACCEPTED</option>


                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-10" id="hide_all">
                                                                                            <div class="form-group">
                                                                                                <label class="form-label" for="fva-message">Rejection Reason </label>
                                                                                                <div class="form-control-wrap">
                                                                                                    <textarea class="form-control form-control-sm" id="message" name="reason" placeholder="Write your reject reason"></textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                        <input type="hidden" class="form-control" name="maker" value="<?php echo $email; ?>">
                                                                                        <input type="hidden" class="form-control" name="id" value="<?= $pop['id'] ?>">

                                                                                        <div class="col-12">
                                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                                <li>
                                                                                                    <input type="hidden" name="verify_pop" value="true">
                                                                                                    <button type="button" class="btn btn-primary" name="verify_pop" onClick="verifyPOP('<?= $pop["id"] ?>')">Submit</button>
                                                                                                </li>

                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div><!-- .modal-body -->
                                                                            <div class="modal-footer bg-light">
                                                                                <span class="sub-text"></span>
                                                                            </div>
                                                                        </div><!-- .modal-content -->
                                                                    </div><!-- .modal-dialog -->
                                                                </div><!-- .modal -->


                                                            <?php endforeach; ?>

                                                        <?php else : ?>

                                                            <?= $queries['message'] ?>

                                                        <?php endif; ?>


                                                </tbody>
                                            </table>
                                            <div class="col-sm-4" style="padding: 0;">
                                                <p>Showing <?= $paginate->offset() + 1 ?> to <?= ($paginate->offset() + 20) > $items_total_count ? $items_total_count : $paginate->offset() + 20 ?> of <?= $items_total_count ?> entries</p>
                                            </div>
                                            <div class="col-sm-8 tp-pagination" style="padding: 0;">
                                                <ul class="pagination pull-right" style="margin: 0;">
                                                    <?php if ($paginate->page_total() > 1) : ?>
                                                        <?php if ($paginate->has_previous()) : ?>
                                                            <li>
                                                                <a href="portal/billing-officer/bank-pop?page=<?php echo $paginate->previous(); ?>" aria-label="Previous"> <span aria-hidden="true">Previous</span> </a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <?php
                                                        for ($i = 1; $i <= $paginate->page_total(); $i++) {
                                                            if ($paginate->current_page > 5 && $i == 1) {
                                                                echo "<li><a href='portal/billing-officer/bank-pop?page={$i}'>{$i}</a></li><li><a>...</a></li>";
                                                            }
                                                            if ($i == $paginate->current_page) {
                                                                echo "<li class='active'><a href='portal/billing-officer/bank-pop?page={$i}'>{$i}</a></li>";
                                                            } elseif ($i > ($paginate->current_page - 4) && $i < ($paginate->current_page + 4) && $i != $paginate->current_page) {
                                                                echo "<li><a href='portal/billing-officer/bank-pop?page={$i}'>{$i}</a></li>";
                                                            }
                                                            if (($paginate->current_page + 4) < $i && $i == $paginate->page_total()) {
                                                                echo "<li><a>...</a></li><li><a href='portal/billing-officer/bank-pop?page={$i}'>{$i}</a></li>";
                                                            }
                                                        }
                                                        ?>
                                                        <?php if ($paginate->has_next()) : ?>
                                                            <li>
                                                                <a href="portal/billing-officer/bank-pop?page=<?php echo $paginate->next(); ?>" aria-label="Next"> <span aria-hidden="true">NEXT</span> </a>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>

                                        </div>
                                    </div><!-- .card-preview -->
                                </div> <!-- nk-block -->


                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; Copyright Ecocash Holdings Zimbabwe 2022 <a href="https://www.ecocashholdings.co.zw/" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAssignAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully asigned query</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failCurrencyAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='updateCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully added currency</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='addCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully assigned query to billing officer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='editCurrencyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successverifyEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                            <div class="caption-text">You’ve successfully Reviewed Proof of Payment</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Fail  Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="verifyfailEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                            <div class='verifyResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function reason() {
            document.getElementById('hide_all').style.display = 'none';
        }

        function getValue() {
            const user_value = document.getElementById("user_value").value;
            console.log(user_value);
            if (user_value == "REJECTED") {
                document.getElementById('hide_all').style.display = 'inline-block';


            } else if (user_value == "") {
                document.getElementById('hide_all').style.display = 'none';
            } else {
                document.getElementById('hide_all').style.display = 'none';
            }
        }
    </script>
    <!-- BEGIN: AJAX CALLS-->


    <script>
        //verify pop
        function verifyPOP(id) {
            $.ajax({
                type: "POST",
                url: "portal/billing-officer/controller/process.php",
                data: $('form#verify-pop' + id).serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    if (json.status == "SUCCESS") {
                        $('#verify-pop' + id).modal('hide');
                        $("#successverifyEditAlert").modal('show');
                        setTimeout(function() {
                            window.location = "portal/billing-officer/bank-pop";
                        }, 2000);

                    } else {
                        $('.verifyResponse').empty();
                        $('.verifyResponse').append(json.message);
                        $('#verify-tax' + id).modal('hide');
                        $("#verifyfailEditAlert").modal('show');
                        $("#verifyfailEditAlert").on("hidden.bs.modal", function() {
                            $(".verifyResponse").html("");
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.verifyResponse').empty();
                    $('.verifyResponse').append(errorThrown);
                    $('#verify-tax' + id).modal('hide');
                    $("#verifyfailEditAlert").modal('show');
                    $("#verifyfailEditAlert").on("hidden.bs.modal", function() {
                        $(".verifyResponse").html("");
                    });
                }
            });
        }
    </script>





    <!-- JavaScript -->
    <?php require_once('includes/footer.php'); ?>
</body>

</html>