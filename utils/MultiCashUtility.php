<?php
error_reporting(0);
// error_reporting(E_ALL);
include_once('curl_url.php');
include_once('common.php');
$GOOGLE_API_KEY  = "AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs";
$secret = '6LeUkr8ZAAAAAPLjUQfjU8takt8g7rxonze_GVIO';


function getAuthToken()
{
    global $auth_url;
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_URL => $auth_url . 'v1/token?scope=read&username=user_vaya&password=p_@55_304_yt&client_id=edx2c30lorg&client_secret=1op9tfxy7&grant_type=password',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $res_arr = json_decode($response, false);
        return $res_arr->access_token;
    }
}


function callWebApiPost($jsonData, $curl_url)
{
    $ch = curl_init($curl_url);
    $jsonDataEncoded = json_encode($jsonData);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);

    return $result;
}

function callWebApiDelete($curl_url)
{
    $ch = curl_init($curl_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);

    return $result;
}

function callWebApiGet($curl_url)
{
    $ch = curl_init($curl_url);
    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function callWebBillingsApiGet($email)
{

    $curl = curl_init();



    curl_setopt_array($curl, [

        CURLOPT_URL => "https://dev.vayaafrica.com/vaya-api-gateway/vaya-multi-wallet/v1/query/get-queries-by-assigned-to?email=" . $email . "",

        CURLOPT_RETURNTRANSFER => true,

        CURLOPT_ENCODING => "",

        CURLOPT_MAXREDIRS => 10,

        CURLOPT_TIMEOUT => 30,

        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

        CURLOPT_CUSTOMREQUEST => "GET",

        CURLOPT_POSTFIELDS => "",
        $ACC_TOKEN = getAuthToken(),
        CURLOPT_HTTPHEADER => [

            "Content-Type: application/json",
            "authorization: Bearer " . $ACC_TOKEN

     

        ],

    ]);

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}

function getCountQuery()
{

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://dev.vayaafrica.com/vaya-api-gateway/vaya-multi-wallet/v1/query/queries-count-by-query-type?queryType=Overstated",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "{}",
        $ACC_TOKEN = getAuthToken(),
        CURLOPT_HTTPHEADER => [

            "Content-Type: application/json",
            "authorization: Bearer " . $ACC_TOKEN

     

        ],
    ]);

    $response = curl_exec($curl);


    curl_close($curl);
    return $response;
}

function getRCountQuery()
{

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://dev.vayaafrica.com/vaya-api-gateway/vaya-multi-wallet/v1/query/queries-count-by-query-type?queryType=Reversal",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "{}",
        $ACC_TOKEN = getAuthToken(),
        CURLOPT_HTTPHEADER => [

            "Content-Type: application/json",
            "authorization: Bearer " . $ACC_TOKEN

     

        ],
    ]);

    $response = curl_exec($curl);


    curl_close($curl);
    return $response;
}

// function getWallets(){

//     $curl = curl_init();

// curl_setopt_array($curl, [
//   CURLOPT_URL => "https://dev.vayaafrica.com/vaya-multi-currency-wallet-backend/vaya-multi-wallet/v1/wallet/get-all-wallets?page=0&size=5",
//   CURLOPT_RETURNTRANSFER => true,
//   CURLOPT_ENCODING => "",
//   CURLOPT_MAXREDIRS => 10,
//   CURLOPT_TIMEOUT => 30,
//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//   CURLOPT_CUSTOMREQUEST => "GET",
//   CURLOPT_POSTFIELDS => "{}",
//   CURLOPT_HTTPHEADER => [
//     "Content-Type: application/json"
//   ],
// ]);

// $response = curl_exec($curl);


// curl_close($curl);

// return $response;
// }

function getWallets($page, $size)
{

    global $base_url;
    global $get_all_wallets;
    $url = $base_url . $get_all_wallets . "?page=" . $page . "&size=" . $size;

    $path = str_replace(" ", "%20", $url);

    $ch = curl_init($path);

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}
function getWWithdrawals($page, $size)
{

    global $base_url;
    global $wallet_withdrawals;
    $url = $base_url . $wallet_withdrawals . "?page=" . $page . "&size=" . $size;

    $path = str_replace(" ", "%20", $url);

    $ch = curl_init($path);

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}

function getQuery($page, $size)
{

    global $base_url;
    global $get_all_queries;
    $url = $base_url . $get_all_queries . "?page=" . $page . "&size=" . $size;

    $path = str_replace(" ", "%20", $url);

    $ch = curl_init($path);

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));
    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}

function getPOP($page, $size)
{

    global $base_url;
    global $get_all_pops;
    $url = $base_url . $get_all_pops . "?page=" . $page . "&size=" . $size;

    $path = str_replace(" ", "%20", $url);

    $ch = curl_init($path);

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}
//Query Types

function getAllQueryTypes()
{
    global $base_url;
    global $all_query_types;

    return callWebApiGet($base_url . $all_query_types);
}



function addQueryType($description, $maker, $queryType, $queryTypeStatus)
{

    $jsonData = array(
        'description' => $description,
        'maker' => $maker,
        'queryType' => $queryType,
        'queryTypeStatus' => $queryTypeStatus
    );

    global $base_url;
    global $create_query_type;
    return callWebApiPost($jsonData, $base_url . $create_query_type);
}

function  editQueryType($id, $description, $queryType, $queryTypeStatus)
{

    $jsonData = array(
        'id' => $id,
        'description' => $description,
        'queryType' => $queryType,
        'queryTypeStatus' => $queryTypeStatus
    );
    global $base_url;
    global $update_query_type;
    return callWebApiPost($jsonData, $base_url . $update_query_type);
}

//Currency API
function getAllCurrency()
{
    global $base_url;
    global $all_currencies;

    return callWebApiGet($base_url . $all_currencies);
}

//wallet withdrawals API
function countAllWallets()
{
    global $base_url;
    global $count_withdrawals_wallets;

    return callWebApiGet($base_url . $count_withdrawals_wallets);
}

function addCurrency($ccy, $description, $maker, $status)
{

    $jsonData = array(
        'ccy' => $ccy,
        'description' => $description,
        'maker' => $maker,
        'status' => $status
    );

    global $base_url;
    global $create_currency;
    return callWebApiPost($jsonData, $base_url . $create_currency);
}

function  editCurrency($id, $c_type, $description, $checker, $status)
{

    $jsonData = array(
        'id' => $id,
        'c_type' => $c_type,
        'description' => $description,
        'checker' => $checker,
        'status' => $status
    );
    global $base_url;
    global $update_currency;
    return callWebApiPost($jsonData, $base_url . $update_currency);
}

//Deductions API
function getAllDeductions()
{
    global $base_url;
    global $all_deductions;

    return callWebApiGet($base_url . $all_deductions);
}

//editDeduction

function  AddDeduction($value, $trip_type, $maker)
{

    $jsonData = array(

        'value' => $value,
        'trip_type' => $trip_type,
        'maker' => $maker

    );
    global $base_url;
    global $update_deduction;
    return callWebApiPost($jsonData, $base_url . $update_deduction);
}

//Configs API
function getAllConfigs()
{
    global $base_url;
    global $all_configs;

    return callWebApiGet($base_url . $all_configs);
}


function addConfig($ccy, $name, $value, $marker)
{

    $jsonData = array(
        'ccy' => $ccy,
        'name' => $name,
        'value' => $value,
        'marker' => $marker

    );


    global $base_url;
    global $create_config;
    return callWebApiPost($jsonData, $base_url . $create_config);
}

//Payment Mode API
function getAllPaymentModes()
{
    global $base_url;
    global $all_payment_modes;

    return callWebApiGet($base_url . $all_payment_modes);
}


function addPaymentMode($maker, $name, $value)
{

    $jsonData = array(
        'maker' => $maker,
        'name' => $name,
        'value' => $value

    );

    global $base_url;
    global $create_payment_mode;
    return callWebApiPost($jsonData, $base_url . $create_payment_mode);
}

function  editPaymentMode($id, $name, $value, $checker, $status)
{

    $jsonData = array(
        'id' => $id,
        'name' => $name,
        'value' => $value,
        'checker' => $checker,
        'status' => $status
    );
    global $base_url;
    global $update_payment_mode;
    return callWebApiPost($jsonData, $base_url . $update_payment_mode);
}


//Transaction Type API
function getAllTransactionTypes()
{
    global $base_url;
    global $all_trans_types;

    return callWebApiGet($base_url . $all_trans_types);
}


function addTransactionType($description, $maker, $status, $ttype)
{

    $jsonData = array(
        'description' => $description,
        'maker' => $maker,
        'status' => $status,
        'ttype' => $ttype
    );

    global $base_url;
    global $create_trans_type;
    return callWebApiPost($jsonData, $base_url . $create_trans_type);
}


function  editTransactionType($id, $description, $status, $ttype, $checker)
{

    $jsonData = array(
        'id' => $id,
        'description' => $description,
        'status' => $status,
        'ttype' => $ttype,
        'checker' => $checker

    );
    global $base_url;
    global $update_trans_type;
    return callWebApiPost($jsonData, $base_url . $update_trans_type);
}

//Query API 
function getAllQueries($page, $size)
{
   

      return getQuery($page, $size);
}


function  rejectAcceptQuery($id, $acceptRejectStatus, $queryNum, $reject_reason, $tripId, $checker)
{

    $jsonData = array(
        'id' => $id,
        'acceptRejectStatus' => $acceptRejectStatus,
        'queryNum' => $queryNum,
        'reject_reason' => $reject_reason,
        'tripId' => $tripId,
        'checker' => $checker
    );
    global $base_url;
    global $reject_accept_query;
    return callWebApiPost($jsonData, $base_url . $reject_accept_query);
}

function  AssignQuery($id, $assigned_to)
{

    $jsonData = array(
        'id' => $id,
        'assigned_to' => $assigned_to
    );
    global $base_url;
    global $assign_billing_officer;
    return callWebApiPost($jsonData, $base_url . $assign_billing_officer);
}


function  verifyQuery($id, $accNumber, $assignedTo, $closedBy, $distance, $drAccount, $partnerId, $payment_mode, $queryNum, $queryStatus, $reject_reason, $tripId)
{

    $jsonData = array(
        'id' => $id,
        'accNumber' => $accNumber,
        'assignedTo' => $assignedTo,
        'closedBy' => $closedBy,
        'distance' => $distance,
        'drAccount' => $drAccount,
        'partnerId' => $partnerId,
        'payment_mode' => $payment_mode,
        'queryNum' => $queryNum,
        'queryStatus' => $queryStatus,
        'reject_reason' => $reject_reason,
        'tripId' => $tripId

    );
    global $base_url;
    global $verify_query;
    return callWebApiPost($jsonData, $base_url . $verify_query);
}

function CountQuery()
{


    return getCountQuery();
}

function RCountQuery()
{


    return getRCountQuery();
}

function  verifyTax($id, $description, $document_status, $maker, $tax_clearance_number)
{

    $jsonData = array(
        'id' => $id,
        'description' => $description,
        'document_status' => $document_status,
        'maker' => $maker,
        'tax_clearance_number' => $tax_clearance_number,


    );
    global $base_url;
    global $verify_tax;
    return callWebApiPost($jsonData, $base_url . $verify_tax);
}

function  verifyPOP($id,$reason,$status,$maker)
{

    $jsonData = array(
        'id' => $id,
        'reason' => $reason,
        'status' => $status,
        'maker' => $maker


    );
    global $base_url;
    global $verify_pop;
    return callWebApiPost($jsonData, $base_url . $verify_pop);
}

//Billing Oficcer API 
function getAllBillingOfficers()
{
    global $base_url;
    global $all_biling_officers;

    return callWebApiGet($base_url . $all_biling_officers);
}


//Billing Query API 
// function getAllBillingQuery($email){

//     global $base_url;
//     global $get_all_query;

//     return callWebApiGet($base_url.$get_all_query.$email);
//  }



function getAllBillingQuery($email)
{

    return callWebBillingsApiGet($email);
}



//Taxations
function getAllTaxClearances()
{
    global $base_url;
    global $get_all_taxations;

    return callWebApiGet($base_url . $get_all_taxations);
}

//Wallet
// function getAllWallets()
// {


//     return getWallets();
// }

function getAllWallets($page, $size)
{

    return getWallets($page, $size);
}

function getWalletWithdrawals($page,$size)
{
    

    return getWWithdrawals($page,$size);
}

function getAllPOP($page, $size)
{

    return getPOP($page, $size);
}
function  editWallet($id, $accStatus, $checker, $taxStatus)
{

    $jsonData = array(
        'id' => $id,
        'accStatus' => $accStatus,
        'checker' => $checker,
        'taxStatus' => $taxStatus
    );
    global $base_url;
    global $update_wallet;
    return callWebApiPost($jsonData, $base_url . $update_wallet);
}
//Withdrawals
function getAllWithdrawals()
{
    global $base_url;
    global $get_all_withdrawals;

    return callWebApiGet($base_url . $get_all_withdrawals);
}


function  verifyRequest($id, $addlText, $authStat)
{

    $jsonData = array(
        'id' => $id,
        'addlText' => $addlText,
        'authStat' => $authStat


    );
    global $base_url;
    global $verify_requests;
    return callWebApiPost($jsonData, $base_url . $verify_requests);
}

//Reports
function accountWallet($accNumber,$page, $size)

{



    global $base_url;

    global $account_transaction;

    $url = $base_url . $account_transaction . "?accNumber=" . $accNumber . "&page=" . $page . "&size=" . $size;



    $path = str_replace(" ", "%20", $url);

    $ch = curl_init($path);

    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    $ACC_TOKEN = getAuthToken();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer ' . $ACC_TOKEN));

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;

}

//partner wallet statement
// function AccountWallet($page,$size,$accNumber)
// {
//     global $base_url;
//     global $account_transaction;

//     return callWebApiGet($base_url . $account_transaction."accNumber=".$accNumber."&page=".$page."&size=".$size);
// }

function getInstantTransfers()
{
    global $base_url;
    global $instant_transfer;

    return callWebApiGet($base_url . $instant_transfer);
}

function getDepositReports()
{
    global $base_url;
    global $deposit_report;

    return callWebApiGet($base_url . $deposit_report);
}

function  VayaCommission($ccy, $paymentMode)
{

    $jsonData = array(
        'ccy' => $ccy,
        'paymentMode' => $paymentMode
    );
    global $base_url;
    global $vaya_commission;
    return callWebApiPost($jsonData, $base_url . $vaya_commission);
}

function adminLogin($email, $password)
{
    $jsonData = array(
        'email' => $email,
        'password' => $password
    );
    global $base_url;
    global $admin_login;
    return callWebApiPost($jsonData, $base_url . $admin_login);
}

//count query
function CountQueries()
{
    global $base_url;
    global $count_query;

    return callWebApiGet($base_url . $count_query);
}

//count pop
function CountPOPs()
{
    global $base_url;
    global $count_pop;

    return callWebApiGet($base_url . $count_pop);
}
//count Partner Wallet Statemnt transaction
function CountTransactions($accNumber)
{
    global $base_url;
    global $count_partner_wallet;

    return callWebApiGet($base_url . $count_partner_wallet."accNumber=".$accNumber);
}

//count wallet
function CountWallets()
{
    global $base_url;
    global $count_wallets;

    return callWebApiGet($base_url . $count_wallets);
}



