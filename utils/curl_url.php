<?php

$auth_url ="https://dev.vayaafrica.com/vaya-api-gateway/";
//$auth_url ="https://vayaafrica.com/vaya-api-gateway/";
$base_url ="https://dev.vayaafrica.com/vaya-api-gateway/vaya-multi-wallet/v1/";
//$base_url ="https://vayaafrica.com/vaya-api-gateway/vaya-multi-wallet/v1/";



//Query Type API
$all_query_types ='query-type/get-all-query-types';
$create_query_type ='query-type/create-query-type';
$update_query_type='query-type/update-query-type';

//Currency API
$all_currencies = 'currency/get-all-currencies';
$create_currency = 'currency/create-currency';
$update_currency = 'currency/update-currency';


//Deductions API
$all_deductions = 'deduction/get-all-deductions';
$update_deduction = 'deduction/update-deduction';

//Configs API
$all_configs = 'config/get-all-configs';
$create_config = 'config/create-config-type';

//Payment Mode
$all_payment_modes = 'get-all-payment-modes';
$create_payment_mode = 'create-payment-mode';
$update_payment_mode = 'update-payment-mode';

//Transactions Type API
$all_trans_types ='transaction-type/get-all-transaction-types';
$create_trans_type ='transaction-type/create-transaction-type';
$update_trans_type ='transaction-type/update-transaction-type';

//Query API
$get_all_queries = 'query/get-all-queries';
$reject_accept_query = 'query/approve-reject-query';
$assign_billing_officer = 'query/assign-billing-officer';
$verify_query ='query/update-query';
$count_queries = 'query/queries-count-by-query-type/';
$count_query = 'query/count-all-queries';

//Billing Officer
$all_biling_officers ='get-all-billing-officers';
$get_all_query ='query/get-queries-by-assigned-to/';

//Taxation
$get_all_taxations = 'tax-clearance/get-all-tax-clearances';
$verify_tax = 'tax-clearance/verify-tax-clearance';

//wallet
$get_all_wallets = 'wallet/get-all-wallets';
$update_wallet = 'wallet/update-wallet';
$count_wallets = 'wallet/count-all-wallets';

//Withdrawals
$get_all_withdrawals = 'withdrawal-request/get-all-withdrawal-requests';
$verify_requests ='transaction/pwd-approval';

//POP
$get_all_pops = 'pop/get-all-bank-pops';
$verify_pop = 'wallet/review-bank-pop';
$count_pop = 'pop/count-all-bank-pops';

//Reports
$wallet_withdrawals = 'report/wallet-withdrawal-report';
$instant_transfer ='report/instant-transfer';
$deposit_report ='report/deposit-report';
$vaya_commission = 'reports/commission';
$count_withdrawals_wallets ='report/count-all-wallet-withdrawals';
$count_partner_wallet = 'transaction/count-account-transaction?';
$account_transaction = 'transaction/account-transaction';
?>
