FROM php:7.4.9-apache
RUN apt-get update && apt-get install -y git
RUN docker-php-ext-install pdo pdo_mysql mysqli
RUN a2enmod rewrite
RUN a2enmod headers

COPY ./php.ini /usr/local/etc/php/
# COPY .htaccess /etc/apache2/
# COPY ./apache2.conf /etc/apache2/

EXPOSE 80
EXPOSE 443
